from keras.models import Sequential, Model
from keras.layers import Lambda, Input, Dense, Conv2D, Flatten, Reshape, Conv2DTranspose, GaussianNoise
from keras.losses import mse, binary_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras import backend as K
from keras.callbacks import TensorBoard
from keras import regularizers

import numpy as np
import matplotlib.pyplot as plt

import os
import functools
import time


original_dim = (130, 73, 3)
cropped_dim = (105, 73, 3)

# network parameters
input_shape = original_dim #(image_size, image_size, 1)
input_size = original_dim[0]*original_dim[1]*original_dim[2]
batch_size = 128
kernel_size = 3
filters = 32
latent_dim = 32 # dimension of encoding space
epochs = 140
rgzer_existence = True
if rgzer_existence == True:
    rgzer = regularizers.l2(0.001)
else:
    rgzer = None


#### Directories
dir_screenshots_train = "data\\screenshots\\train"
dir_screenshots_test = "data\\screenshots\\test"
filename_avg = 'avg.npy'


#### data preprocessing

def noisy_gen(gen):
    ## put this between data generator and network to add noise to the data (not label)
    for i in gen:
        noise_amt = .1
        noise = np.random.random(i[0].shape)*noise_amt-noise_amt/2  # generate noise
        tensor = i[0]
        tensor_noisy = tensor + noise
        # tensor_noisy = np.clip(tensor_noisy, -1, 1)
        yield tensor_noisy, tensor

avg = np.load(filename_avg)

#### crop the image
def crop_scale_tensor(tensor):
    ## scale tensor to [0,1]
    tensor = tensor * 1./255 
    ## black out uninteresting parts
    tensor[cropped_dim[0]:,:,:] = 0
    tensor[:,cropped_dim[1]:,:] = 0
    return tensor

avg = crop_scale_tensor(avg) # scale and crop the average

#### remove the average (precalculated from a bunch of images)
def remove_avg(tensor):
    tensor -= avg
    return tensor

def preprocess_input(tensor):
    tensor = crop_scale_tensor(tensor)
    tensor = remove_avg(tensor)
    return tensor


## data loading
train_datagen = ImageDataGenerator(preprocessing_function=preprocess_input)

train_generator = train_datagen.flow_from_directory(
    directory=dir_screenshots_train,
    target_size=(original_dim[0], original_dim[1]),
    color_mode="rgb",
    batch_size=batch_size,
    class_mode="input",
) 

train_generator_noisy = noisy_gen(train_generator) ## take noisy version

test_datagen = ImageDataGenerator(preprocessing_function=preprocess_input)

test_generator = test_datagen.flow_from_directory(
    directory=dir_screenshots_test,
    target_size=(original_dim[0], original_dim[1]),
    color_mode="rgb",
    batch_size=batch_size,
    class_mode="input"
) 

test_generator_noisy = noisy_gen(test_generator) ## take noisy version

#### NN part

def sampling(args):
    """Reparameterization trick by sampling from an isotropic unit Gaussian.
    # Arguments
        args (tensor): mean and log of variance of Q(z|X)
    # Returns
        z (tensor): sampled latent vector
    """

    z_mean, z_log_var = args
    batch = K.shape(z_mean)[0]
    dim = K.int_shape(z_mean)[1]
    # by default, random_normal has mean = 0 and std = 1.0
    epsilon = K.random_normal(shape=(batch, dim))
    return z_mean + K.exp(0.5 * z_log_var) * epsilon


# build encoder model
inputs = Input(shape=input_shape, name='encoder_input')
#x = GaussianNoise(.2)(inputs) # Add gaussian noise to the inputs for less overfitting
x = inputs
for i in range(3):
    print("shape")
    print(K.int_shape(x))
    filters *= 2
    x = Conv2D(filters=filters,
               kernel_size=kernel_size,
               activation='relu',
               strides=2,
               padding='same',
               kernel_regularizer=rgzer)(x)


# shape info needed to build decoder model
shape = K.int_shape(x)

x = Flatten()(x)
x = Dense(256, activation='relu',
               kernel_regularizer=rgzer)(x)
x = Dense(256, activation='relu',
               kernel_regularizer=rgzer)(x)

z_mean = Dense(latent_dim, name='z_mean')(x)
z_log_var = Dense(latent_dim, name='z_log_var')(x)

# use reparameterization trick to push the sampling out as input
z = Lambda(sampling, output_shape=(latent_dim,), name='z')([z_mean, z_log_var])
#z = Lambda(sampling, output_shape=(latent_dim,), name='z', arguments = ([z_mean, z_log_var]))

# instantiate encoder model
encoder = Model(inputs, [z_mean, z_log_var, z], name='encoder')
encoder.summary()


# build decoder model
latent_inputs = Input(shape=(latent_dim,), name='z_sampling')
x = Dense(256, activation='relu',
               kernel_regularizer=rgzer)(latent_inputs)
x = Dense(shape[1] * shape[2] * shape[3], activation='relu',
               kernel_regularizer=rgzer)(x)
x = Reshape((shape[1], shape[2], shape[3]))(x)

x = Conv2DTranspose(filters=filters,
                    kernel_size=kernel_size,
                    activation='relu',
                    strides=2,
                    output_padding=0,
                    padding='same',
                    kernel_regularizer=rgzer)(x)
print("shape2")
print(K.int_shape(x))
filters //= 2

x = Conv2DTranspose(filters=filters,
                    kernel_size=kernel_size,
                    activation='relu',
                    strides=2,
                    output_padding=0,
                    padding='same',
                    kernel_regularizer=rgzer)(x)
print("shape2")
print(K.int_shape(x))
filters //= 2

x = Conv2DTranspose(filters=filters,
                    kernel_size=kernel_size,
                    activation='relu',
                    strides=2,
                    output_padding=(1,0), # if gives size errors, try different padding
                    padding='same',
                    kernel_regularizer=rgzer)(x)               
print("shape2")
print(K.int_shape(x))         
# for i in range(2):
#     x = Conv2DTranspose(filters=filters,
#                         kernel_size=kernel_size,
#                         activation='relu',
#                         strides=2,
#                         output_padding=0,
#                         padding='same')(x)
    # print("shape2")
    # print(K.int_shape(x))
    # filters //= 2

outputs = Conv2DTranspose(filters=3,
                          kernel_size=kernel_size,
                          activation='sigmoid',
                          padding='same',
                          name='decoder_output')(x)

# x = Flatten()(x)
# outputs = Dense(input_size, activation='relu')(x)
# outputs = Reshape((original_dim[0],original_dim[1],original_dim[2]))(outputs)

decoder = Model(latent_inputs, outputs, name='decoder')
decoder.summary()

# instantiate VAE model
outputs = decoder(encoder(inputs)[2])
vae = Model(inputs, outputs, name='vae_mlp')




# VAE loss = mse_loss or xent_loss + kl_loss

def my_vae_loss(inputs, outputs):

    beta = 2.4 # 1.2
    reconstruction_loss = binary_crossentropy(K.flatten(inputs), K.flatten(outputs))
    reconstruction_loss *= input_size
    kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
    kl_loss = K.sum(kl_loss, axis=-1)
    kl_loss *= -0.5
    kl_loss *= beta
    vae_loss = K.mean(reconstruction_loss + kl_loss)
    #vae.add_loss(vae_loss)
    return vae_loss




vae.compile(optimizer='rmsprop', loss=my_vae_loss)
vae.summary()

# if args.weights:
#     vae.load_weights(args.weights)
# else:
#     # train the autoencoder

now = time.strftime("%Y%m%d%H%M%S")

vae.fit_generator(train_generator_noisy,
        steps_per_epoch=50,
        epochs=epochs,
        validation_data=test_generator_noisy,
        validation_steps=10,
        callbacks=[TensorBoard(log_dir='tmp\\tensorboard\\'+now)])

# vae.fit(x_train,
#         epochs=epochs,
#         batch_size=batch_size,
#         validation_data=(x_test, None))
vae.save_weights('vae_cnn_mnist_24.h5')



# def plot_results(models,
#                  data,
#                  batch_size=128,
#                  model_name="vae_mnist"):
#     """Plots labels and MNIST digits as function of 2-dim latent vector
#     # Arguments
#         models (tuple): encoder and decoder models
#         data (tuple): test data and label
#         batch_size (int): prediction batch size
#         model_name (string): which model is using this function
#     """

#     encoder, decoder = models
#     x_test, y_test = data
#     os.makedirs(model_name, exist_ok=True)

#     filename = os.path.join(model_name, "vae_mean.png")
#     # display a 2D plot of the digit classes in the latent space
#     z_mean, _, _ = encoder.predict(x_test,
#                                    batch_size=batch_size)
#     plt.figure(figsize=(12, 10))
#     plt.scatter(z_mean[:, 0], z_mean[:, 1], c=y_test)
#     plt.colorbar()
#     plt.xlabel("z[0]")
#     plt.ylabel("z[1]")
#     plt.savefig(filename)
#     plt.show()

#     filename = os.path.join(model_name, "digits_over_latent.png")
#     # display a 30x30 2D manifold of digits
#     n = 30
#     digit_size = 28
#     figure = np.zeros((digit_size * n, digit_size * n))
#     # linearly spaced coordinates corresponding to the 2D plot
#     # of digit classes in the latent space
#     grid_x = np.linspace(-4, 4, n)
#     grid_y = np.linspace(-4, 4, n)[::-1]

#     for i, yi in enumerate(grid_y):
#         for j, xi in enumerate(grid_x):
#             z_sample = np.array([[xi, yi]])
#             x_decoded = decoder.predict(z_sample)
#             digit = x_decoded[0].reshape(digit_size, digit_size)
#             figure[i * digit_size: (i + 1) * digit_size,
#                    j * digit_size: (j + 1) * digit_size] = digit

#     plt.figure(figsize=(10, 10))
#     start_range = digit_size // 2
#     end_range = n * digit_size + start_range + 1
#     pixel_range = np.arange(start_range, end_range, digit_size)
#     sample_range_x = np.round(grid_x, 1)
#     sample_range_y = np.round(grid_y, 1)
#     plt.xticks(pixel_range, sample_range_x)
#     plt.yticks(pixel_range, sample_range_y)
#     plt.xlabel("z[0]")
#     plt.ylabel("z[1]")
#     plt.imshow(figure, cmap='Greys_r')
#     plt.savefig(filename)
#     plt.show()




# x_test, y_test = next(train_generator)
# models = (encoder, decoder)
# data = (x_test, y_test)
# plot_results(models, data, batch_size=batch_size, model_name="vae_cnn")


# ### plotting the generator:


# # x_train, y_train = next(train_generator)
# # x_train, y_train = scale_noise_processing(x_train)

# # fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(5, 3))
# # axes[0].imshow(x_batch[0])
# # axes[1].imshow(y_batch[0])
# # fig.tight_layout()

# # plt.show()
# # 
# # plt.imshow(x_batch[0])
# # plt.imshow(y_batch[0])
# # plt.show()

# # NN