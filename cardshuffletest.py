import pyautogui as pgui
import cv2
import time
import numpy as np
from queue import Queue
import threading

button_bluestacks_home = "game_buttons\\bluestacks_home.bmp"
menu_battle_tab = "game_buttons\\menu_battle_tab.bmp"
menu_battle_text  = "game_buttons\\menu_battle_text.bmp"
menu_card_collection = "game_buttons\\menu_card_collection.bmp"
menu_cards_tab = "game_buttons\\menu_cards_tab.bmp"
menu_cards_use = "game_buttons\\menu_cards_use.bmp"

REGION_SIZE = (292, 520) # region sizes of game

origin = pgui.locateOnScreen(button_bluestacks_home)
origin = (origin.left, origin.top + origin.height)
total_region = (origin[0], origin[1], REGION_SIZE[0], REGION_SIZE[1]) # left, top, width, height
card_region = (70, 435, 280-70, 490-435) # region of cards (left, top, width, height)
battle_region = (42, 140, 240-42, 290-140) # region of battle (left, top, width, height)
new_cards_region = (10, 56, 213-10, 128-56) # region of pickable cards for switching
old_cards_region = (8, 107, 285-8, 293-107) # region of old cards for switching

def randclick(x, y, xdev=10, ydev=10):
    # to add some deviations in the clicks
    randx = np.random.randint(-xdev,xdev)
    randy = np.random.randint(-ydev,ydev)
    pgui.click(x + randx, y + randy)

def pickRelclick(origin, region):
    # click in a region which is relative to the origin
    # assuming region is in format (left, top, width, height)
    x = np.random.randint(origin[0] + region[0], origin[0] + region[0] + region[2])
    y = np.random.randint(origin[1] + region[1], origin[1] + region[1] + region[3])
    pgui.click(x,y)

def shuffle_cards():
    "shuffle cards while in menu"
    time.sleep(np.random.rand()*2+.5)
    # find and click cards tab
    loc_cards_tab = pgui.locateCenterOnScreen(menu_cards_tab, region=total_region)
    randclick(loc_cards_tab[0],loc_cards_tab[1])
    time.sleep(np.random.rand()*2+.8) # wait between .8 and 2.8 secs

    try:
        # find and click battle text
        loc_battle_text = pgui.locateCenterOnScreen(menu_battle_text, region=total_region)
        randclick(loc_battle_text[0],loc_battle_text[1])
        time.sleep(np.random.rand()*2+.8) # wait between .8 and 2.8 secs

        # find and move mouse to the card collection text
        loc_card_collection = pgui.locateCenterOnScreen(menu_card_collection, region=total_region)
        pgui.moveTo(loc_card_collection[0],loc_card_collection[1])
        # drag mouse the where the battle text was
        pgui.dragTo(loc_battle_text[0],loc_battle_text[1], 1, pgui.easeInOutQuad)

        # pick a new card in the region
        time.sleep(np.random.rand()*2+.8)
        pickRelclick(origin, new_cards_region)

        # find "use" button
        time.sleep(np.random.rand()*2+.8)
        loc_use_button = pgui.locateCenterOnScreen(menu_cards_use, region=total_region, confidence = 0.9)
        randclick(loc_use_button[0],loc_use_button[1])    

        # pick a card to be replaced
        time.sleep(np.random.rand()*2+.8)
        pickRelclick(origin, old_cards_region)
        time.sleep(np.random.rand()*2+.8)
    except:
        pass

    # find and click battle tab
    loc_battle_tab = pgui.locateCenterOnScreen(menu_battle_tab, region=total_region)
    randclick(loc_battle_tab[0],loc_battle_tab[1])
    time.sleep(np.random.rand()*2+.8) # wait between .8 and 2.8 secs


shuffle_cards()

# find "use" button
#time.sleep(np.random.rand()*2+.8)
#print(pgui.locateCenterOnScreen(menu_cards_use, region=total_region))
#randclick(loc_use_button[0],loc_use_button[1]) 