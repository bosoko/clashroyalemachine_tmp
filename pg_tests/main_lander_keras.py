import gym
import matplotlib.pyplot as plt
import numpy as np
from reinforce_keras import Agent

def plotLearning(scores, filename, x=None, window=5):   
    N = len(scores)
    running_avg = np.empty(N)
    for t in range(N):
	    running_avg[t] = np.mean(scores[max(0, t-window):(t+1)])
    if x is None:
        x = [i for i in range(N)]
    plt.ylabel('Score')       
    plt.xlabel('Game')                     
    plt.plot(x, running_avg)
    plt.savefig(filename)


if __name__ == '__main__':
    agent = Agent(ALPHA=0.0005, input_dims = 4, GAMMA = 0.99, n_actions = 2,
                 layer1_size=64, layer2_size=64)

    env = gym.make('CartPole-v0')
    score_history = []

    n_episodes = 2000

    for i in range(n_episodes):
        done = False
        score = 0
        observation = env.reset()
        while not done:
            action = agent.choose_action(observation)
            observation_, reward, done, info = env.step(action)
            agent.store_transition(observation, action, reward)
            observation = observation_
            score += reward
        score_history.append(score)

        agent.learn()

        print('episode', i, 'score %.1f' % score)

    filename = 'lunar_lander.png'
    plotLearning(score_history, filename=filename, window=100)    