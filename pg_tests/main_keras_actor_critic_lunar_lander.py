import gym
from actor_critic_keras import Agent
from utils import plotLearning
import numpy as np 

agent = Agent(alpha = 0.00001, beta = 0.00005, input_dims=4, n_actions=2)

#env = gym.make('LunarLander-v2')
env = gym.make('CartPole-v0')

score_history = []
num_episodes = 2000

for i in range(num_episodes):
    done = False
    score = 0
    observation = env.reset()

    while not done:
        action = agent.choose_action(observation)
        observation_, reward, done, infor = env.step(action)
        agent.learn(observation, action, reward, observation_, done)
        observation = observation_
        score += reward

    score_history.append(score)
    avg_score = np.mean(score_history[-100:])
    print('episode {}, score {} average score {}'.format(i, score, avg_score))

    filename = 'whatever.png'
    plotLearning(score_history, filename=filename, window=100)