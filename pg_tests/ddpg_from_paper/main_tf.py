from ddpg_tf_orig import Agent
import gym
import numpy as np
import matplotlib.pyplot as plt

np.random.seed(0)

def plotLearning(scores, filename, x=None, window=5):   
    N = len(scores)
    running_avg = np.empty(N)
    for t in range(N):
	    running_avg[t] = np.mean(scores[max(0, t-window):(t+1)])
    if x is None:
        x = [i for i in range(N)]
    plt.ylabel('Score')       
    plt.xlabel('Game')                     
    plt.plot(x, running_avg)
    plt.savefig(filename)

env = gym.make('Pendulum-v0')
agent = Agent(alpha=.0001, beta=.001, input_dims = [3],
              tau=.001, env=env, batch_size=64, 
              layer1_size=400, layer2_size=300,
              n_actions=1)
score_history = []
for i in range(1000):
    obs = env.reset()
    done = False
    score = 0
    while not done:
        act = agent.choose_action(obs)
        new_state, reward, done, info = env.step(act)
        agent.remember(obs, act, reward, new_state, int(done))
        agent.learn()
        score += reward
        obs = new_state
    score_history.append(score)
    print('episode {}, score {}, 100 game avg  {}'.format(i, score, np.mean(score_history[-100:])))

flename = 'pendulum.png'
plotLearning(score_history, flename, window = 100)