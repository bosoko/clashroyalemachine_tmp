from hyperopt import Trials, STATUS_OK, tpe
from hyperas import optim
from hyperas.distributions import choice, uniform
import hyperopt

from keras.models import Sequential, Model
from keras.layers import Lambda, Input, Dense, Conv2D, Flatten, Reshape, Conv2DTranspose, GaussianNoise #, Dropout, Activation
from keras.losses import mse, binary_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras import backend as K
from keras.callbacks import TensorBoard, Callback, TerminateOnNaN
from keras import regularizers

import numpy as np
import matplotlib.pyplot as plt

import os
import functools
import time

def data():
    return [[0],[1]]


def create_model():
    """
    Model providing function:

    Create Keras model with double curly brackets dropped-in as needed.
    Return value has to be a valid python dictionary with two customary keys:
        - loss: Specify a numeric evaluation metric to be minimized
        - status: Just use STATUS_OK and see hyperopt documentation if not feasible
    The last one is optional, though recommended, namely:
        - model: specify the model just created so that we can later use it again.
    """

    
    original_dim = (130, 73, 3)
    cropped_dim = (105, 73, 3)

    # network parameters
    input_shape = original_dim #(image_size, image_size, 1)
    input_size = original_dim[0]*original_dim[1]*original_dim[2]
    batch_size = 128
    kernel_size = 3
    filters = 32
    num_conv_layers = 3
    latent_dim = 40 # dimension of encoding space
    rgzer_existence =  False
    if rgzer_existence == True:
        rgzer = None
    else:
        rgzer = None
    steps_per_epoch =  50
    beta_vae = 1.2
    noise_amt_g = {{randint(8)}}/20
    epochs = 90

    #### Directories
    dir_screenshots_train = "data\\screenshots\\train"
    dir_screenshots_test = "data\\screenshots\\test"
    filename_avg = 'avg.npy'
    now = time.strftime("%Y%m%d%H%M%S")
    log_dir='tmp/tensorboard/'+now


    #### data preprocessing

    def noisy_gen(gen):
        ## put this between data generator and network to add noise to the data (not label)
        for i in gen:
            noise_amt = noise_amt_g
            noise = np.random.random(i[0].shape)*noise_amt-noise_amt/2  # generate noise
            tensor = i[0]
            tensor_noisy = tensor + noise
            # tensor_noisy = np.clip(tensor_noisy, -1, 1)
            yield tensor_noisy, tensor

    avg = np.load(filename_avg)

    #### crop the image
    def crop_scale_tensor(tensor):
        ## scale tensor to [0,1]
        tensor = tensor * 1./255 
        ## black out uninteresting parts
        tensor[cropped_dim[0]:,:,:] = 0
        tensor[:,cropped_dim[1]:,:] = 0
        return tensor

    avg = crop_scale_tensor(avg) # scale and crop the average

    #### remove the average (precalculated from a bunch of images)
    def remove_avg(tensor):
        tensor -= avg
        return tensor

    def preprocess_input(tensor):
        tensor = crop_scale_tensor(tensor)
        tensor = remove_avg(tensor)
        return tensor


    ## data loading
    train_datagen = ImageDataGenerator(preprocessing_function=preprocess_input)

    train_generator = train_datagen.flow_from_directory(
        directory=dir_screenshots_train,
        target_size=(original_dim[0], original_dim[1]),
        color_mode="rgb",
        batch_size=batch_size,
        class_mode="input",
    ) 

    train_generator_noisy = noisy_gen(train_generator) ## take noisy version

    test_datagen = ImageDataGenerator(preprocessing_function=preprocess_input)

    test_generator = test_datagen.flow_from_directory(
        directory=dir_screenshots_test,
        target_size=(original_dim[0], original_dim[1]),
        color_mode="rgb",
        batch_size=batch_size,
        class_mode="input"
    ) 

    test_generator_noisy = noisy_gen(test_generator) ## take noisy version

    #### NN part

    def sampling(args):
        """Reparameterization trick by sampling from an isotropic unit Gaussian.
        # Arguments
            args (tensor): mean and log of variance of Q(z|X)
        # Returns
            z (tensor): sampled latent vector
        """

        z_mean, z_log_var = args
        batch = K.shape(z_mean)[0]
        dim = K.int_shape(z_mean)[1]
        # by default, random_normal has mean = 0 and std = 1.0
        epsilon = K.random_normal(shape=(batch, dim))
        return z_mean + K.exp(0.5 * z_log_var) * epsilon


    # build encoder model
    inputs = Input(shape=input_shape, name='encoder_input')
    #x = GaussianNoise(.2)(inputs) # Add gaussian noise to the inputs for less overfitting
    x = inputs

    
    for i in range(num_conv_layers):
        print("shape")
        print(K.int_shape(x))
        filters *= 2
        x = Conv2D(filters=filters,
                kernel_size=kernel_size,
                activation='relu',
                strides=2,
                padding='same',
                kernel_regularizer=rgzer)(x)


    # shape info needed to build decoder model
    shape = K.int_shape(x)

    x = Flatten()(x)
    x = Dense(256, activation='relu',
                kernel_regularizer=rgzer)(x)
    x = Dense(256, activation='relu',
                kernel_regularizer=rgzer)(x)

    z_mean = Dense(latent_dim, name='z_mean')(x)
    z_log_var = Dense(latent_dim, name='z_log_var')(x)

    # use reparameterization trick to push the sampling out as input
    # note that "output_shape" isn't necessary with the TensorFlow backend
    z = Lambda(sampling, name='z')([z_mean, z_log_var])
    #z = Lambda(sampling, output_shape=(latent_dim,), name='z')([z_mean, z_log_var])
    #z = Lambda(sampling, output_shape=(latent_dim,), name='z', arguments = ([z_mean, z_log_var]))

    # instantiate encoder model
    encoder = Model(inputs, [z_mean, z_log_var, z], name='encoder')
    encoder.summary()


    # build decoder model
    latent_inputs = Input(shape=(latent_dim,), name='z_sampling')
    x = Dense(256, activation='relu',
                kernel_regularizer=rgzer)(latent_inputs)
    x = Dense(shape[1] * shape[2] * shape[3], activation='relu',
                kernel_regularizer=rgzer)(x)
    x = Reshape((shape[1], shape[2], shape[3]))(x)
    if num_conv_layers == 3:
        x = Conv2DTranspose(filters=filters,
                            kernel_size=kernel_size,
                            activation='relu',
                            strides=2,
                            output_padding=0,
                            padding='same',
                            kernel_regularizer=rgzer)(x)
        print("shape2")
        print(K.int_shape(x))
        filters //= 2

    x = Conv2DTranspose(filters=filters,
                        kernel_size=kernel_size,
                        activation='relu',
                        strides=2,
                        output_padding=0,
                        padding='same',
                        kernel_regularizer=rgzer)(x)
    print("shape2")
    print(K.int_shape(x))
    filters //= 2

    x = Conv2DTranspose(filters=filters,
                        kernel_size=kernel_size,
                        activation='relu',
                        strides=2,
                        output_padding=(1,0), # if gives size errors, try different padding
                        padding='same',
                        kernel_regularizer=rgzer)(x)               
    print("shape2")
    print(K.int_shape(x))         
    # for i in range(2):
    #     x = Conv2DTranspose(filters=filters,
    #                         kernel_size=kernel_size,
    #                         activation='relu',
    #                         strides=2,
    #                         output_padding=0,
    #                         padding='same')(x)
        # print("shape2")
        # print(K.int_shape(x))
        # filters //= 2

    outputs = Conv2DTranspose(filters=3,
                            kernel_size=kernel_size,
                            activation='sigmoid',
                            padding='same',
                            name='decoder_output')(x)

    # x = Flatten()(x)
    # outputs = Dense(input_size, activation='relu')(x)
    # outputs = Reshape((original_dim[0],original_dim[1],original_dim[2]))(outputs)

    decoder = Model(latent_inputs, outputs, name='decoder')
    decoder.summary()

    # instantiate VAE model
    outputs = decoder(encoder(inputs)[2])
    vae = Model(inputs, outputs, name='vae_mlp')




    # VAE loss = mse_loss or xent_loss + kl_loss

    def my_vae_loss(inputs, outputs):
        
        beta = beta_vae # 1.2
        reconstruction_loss = binary_crossentropy(K.flatten(inputs), K.flatten(outputs))
        reconstruction_loss *= input_size
        kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
        kl_loss = K.sum(kl_loss, axis=-1)
        kl_loss *= -0.5
        kl_loss *= beta
        vae_loss = K.mean(reconstruction_loss + kl_loss)
        #vae.add_loss(vae_loss)
        return vae_loss




    vae.compile(optimizer='rmsprop', loss=my_vae_loss)
    vae.summary()

    # if args.weights:
    #     vae.load_weights(args.weights)
    # else:
    #     # train the autoencoder

    
    print("now", now)
    hparams_names = "batch_size, filters, latent_dim, rgzer, steps_per_epoch, beta_vae, num_conv_layers, noise_amt_g"
    print(hparams_names)
    hparams = (batch_size, filters, latent_dim, rgzer, steps_per_epoch, beta_vae, num_conv_layers, noise_amt_g)
    print(hparams)
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    with open(log_dir+'/hparams.txt', 'w+') as f:
        f.write(hparams_names + '\n')
        f.write(str(hparams))
    #np.savetxt(log_dir+'hparams.txt', np.array(hparams), fmt='%1c')


    result = vae.fit_generator(train_generator_noisy,
        steps_per_epoch=steps_per_epoch,
        epochs=epochs,
        validation_data=test_generator_noisy,
        validation_steps=10,
        callbacks=[TerminateOnNaN(), TensorBoard(log_dir=log_dir)])

    # get the lowest validation loss of the training epochs

    validation_loss = np.amin(result.history['val_loss']) 
    print('Best validation loss of epoch:', validation_loss)
    return {'loss': validation_loss, 'status': STATUS_OK, 'model': vae}


#if __name__ == '__main__':
best_run, best_model = optim.minimize(model=create_model,
                                        data=data,
                                        algo=tpe.suggest,
                                        max_evals=57,
                                        trials=Trials())

# X_train, Y_train, X_test, Y_test = data()
# print("Evalutation of best performing model:")
# print(best_model.(X_test, Y_test))
print("Best performing model chosen hyper-parameters:")
print(best_run)
                                    
best_model.save("best_model.h5")
