import pyautogui as pgui
import cv2
import time
import numpy as np
from queue import Queue
import threading

### Random player for clash royale to grab screenshots
### TODO: grab screenshots and save with random names
### TODO: use try/except for startgame?


pgui.FAILSAFE = True

## Buttons
button_bluestacks_home = "game_buttons\\bluestacks_home.bmp"
button_menu = "game_buttons\\menu.bmp"
button_camp = "game_buttons\\camp.bmp"
button_camp_yes = "game_buttons\\camp_yes.bmp"
button_match_exit = "game_buttons\\match_exit.bmp"
button_match_ok = "game_buttons\\match_ok.bmp"
sign_match_time = "game_buttons\\match_time.bmp"
sign_match_overtime = "game_buttons\\match_overtime.bmp"
sign_match_trainer_face = "game_buttons\\match_trainer_face.bmp"
menu_battle_tab = "game_buttons\\menu_battle_tab.bmp"
menu_battle_text  = "game_buttons\\menu_battle_text.bmp"
menu_card_collection = "game_buttons\\menu_card_collection.bmp"
menu_cards_tab = "game_buttons\\menu_cards_tab.bmp"
menu_card_collection = "game_buttons\\menu_card_collection.bmp"
menu_cards_use = "game_buttons\\menu_cards_use.bmp"
menu_close = "game_buttons\\menu_close.bmp"

## Directories
dir_screenshots = "data\\screenshots\\"

REGION_SIZE = (292, 520) # region sizes of game

origin = pgui.locateOnScreen(button_bluestacks_home)
origin = (origin.left, origin.top + origin.height)
total_region = (origin[0], origin[1], REGION_SIZE[0], REGION_SIZE[1]) # left, top, width, height
card_region = (70, 435, 280-70, 490-435) # region of cards (left, top, width, height)
battle_region = (42, 140, 240-42, 290-140) # region of battle (left, top, width, height)
new_cards_region = (10, 56, 285-10, 256-56) # region of pickable cards for switching
old_cards_region = (8, 107, 285-8, 293-107) # region of old cards for switching


"""pgui.moveTo(region[0],region[1])
time.sleep(1)
pgui.moveTo(region[0]+region[2],region[1])
time.sleep(1)
pgui.moveTo(region[0],region[1]+region[3])
time.sleep(1)
pgui.moveTo(region[0]+region[2],region[1]+region[3])
"""

def betterclicker(x,y):
    pgui.moveTo(x,y)
    time.sleep(.1)
    pgui.mouseDown()
    time.sleep(np.random.rand()*.2+.1)
    pgui.mouseUp()

def randclick(x, y, xdev=10, ydev=10):
    # to add some deviations in the clicks
    randx = np.random.randint(-xdev,xdev)
    randy = np.random.randint(-ydev,ydev)
    betterclicker(x + randx, y + randy)

def pickRelclick(origin, region):
    # click in a region which is relative to the origin
    # assuming region is in format (left, top, width, height)
    x = np.random.randint(origin[0] + region[0], origin[0] + region[0] + region[2])
    y = np.random.randint(origin[1] + region[1], origin[1] + region[1] + region[3])
    betterclicker(x,y)



def startgame():
    print("init startgame")
    time.sleep(np.random.rand()*3+3) # wait between 3 and 6 secs, for failsafe
    
    # find and click menu
    print("looking for menu...")
    loc_button_menu = pgui.locateCenterOnScreen(button_menu, region=total_region)
    randclick(loc_button_menu[0],loc_button_menu[1], xdev = 1, ydev = 5)
    time.sleep(np.random.rand()*2+.8) # wait between .8 and 2.8 secs

    # find and click Training Camp
    print("looking for training camp text...")
    loc_button_camp = pgui.locateCenterOnScreen(button_camp, region=total_region)
    randclick(loc_button_camp[0],loc_button_camp[1])
    time.sleep(np.random.rand()*2+.8) # wait between .8 and 2.8 secs

    # find and click Yes for training camp
    print("looking for Yes...")
    loc_button_camp_yes = pgui.locateCenterOnScreen(button_camp_yes, region=total_region)
    randclick(loc_button_camp_yes[0],loc_button_camp_yes[1])
    time.sleep(np.random.rand()*2+.8) # wait between .8 and 2.8 secs

def playgame(queue):
    """
    Plays game by picking random cards and playing them somewhere on the field at random times
    Checks for gameover by looking for exit and ok buttons
    takes screenshots
    """
    done = False
    time.sleep(np.random.rand()*3+5)
    queue.put("start")
    
    while not done:
        
        try:
            
            time.sleep(np.random.rand()*5+2)
            # pick CARD
            print("picking a card")
            pickRelclick(origin, card_region)

            ## Check if the time indicator is there
            try:
                print("looking for time indicator")
                pgui.locateCenterOnScreen(sign_match_time, region=total_region, confidence=0.7)
                print("time indicator is here")
            except:
                # if not, check if overtime indicator is there
                print("time indicator not found, looking for overtime indicator")
                pgui.locateCenterOnScreen(sign_match_overtime, region=total_region, confidence=0.9)
                print("overtime indicator found")

            time.sleep(np.random.rand()+.5)
            # pick REGION
            print("picking a region")
            pickRelclick(origin, battle_region)


        except:
            ## If the x button is not there, 
            ## check 5 times if the OK button is there (match over)
            for _ in range(9):
                try:
                    print("looking for OK button")
                    okButton = pgui.locateCenterOnScreen(button_match_ok, region=total_region)
                    print("button found, clicking it")
                    randclick(okButton[0], okButton[1])
                    done = True
                    queue.put(done)
                    return
                except:
                    time.sleep(3)
                done = True
                
        
def take_screens(region, delay, queue):
    queue.get() # wait for queue to be filled 
    while queue.empty(): # save screenshots with a delay until queue is not empty
        try:
            ## Check if the time indicator is there
            try:
                
                pgui.locateCenterOnScreen(sign_match_time, region=total_region)
            except:
                # if not, check if overtime indicator is there
                pgui.locateCenterOnScreen(sign_match_overtime, region=total_region)
            screenshot_filename = dir_screenshots + str(np.random.randint(1000000000)) + ".bmp"
            screenshot = pgui.screenshot(region=total_region).resize((REGION_SIZE[0]//4, REGION_SIZE[1]//4))
            screenshot.save(screenshot_filename)
            time.sleep(1)
        except:
            pass
    pass

def shuffle_cards():
    "shuffle cards while in menu"
    time.sleep(np.random.rand()*2+.5)
    # find and click cards tab
    try:
        print("shuffling: looking for cards tab")
        loc_cards_tab = pgui.locateCenterOnScreen(menu_cards_tab, region=total_region)
        print("shuffling: found the cards tab")
        randclick(loc_cards_tab[0],loc_cards_tab[1])
        time.sleep(np.random.rand()*2+.8) # wait between .8 and 2.8 secs
    except: 
        pass
    try:
        # find and click battle text
        print("shuffling: looking for cards tab")
        loc_battle_text = pgui.locateCenterOnScreen(menu_battle_text, region=total_region)
        print("shuffling: found cards tab")
        randclick(loc_battle_text[0],loc_battle_text[1])
        time.sleep(np.random.rand()*2+.8) # wait between .8 and 2.8 secs

        # find and move mouse to the card collection text
        print("shuffling: looking for card collection text")
        loc_card_collection = pgui.locateCenterOnScreen(menu_card_collection, region=total_region)
        print("shuffling: found card collection text")
        pgui.moveTo(loc_card_collection[0],loc_card_collection[1])
        # drag mouse the where the battle text was
        print("shuffling: dragging up")
        pgui.dragTo(loc_battle_text[0],loc_battle_text[1], 1, pgui.easeInOutQuad)

        # pick a new card in the region
        time.sleep(np.random.rand()*2+.8)
        print("shuffling: picking a new card")
        pickRelclick(origin, new_cards_region)

        # find "use" button
        time.sleep(np.random.rand()*2+.8)
        print("shuffling: looking for the use button")
        loc_use_button = pgui.locateCenterOnScreen(menu_cards_use, region=total_region, confidence = 0.9)
        print("shuffling: clicking the use button")
        randclick(loc_use_button[0],loc_use_button[1])    

        # pick a card to be replaced
        time.sleep(np.random.rand()*2+.8)
        print("shuffling: picking a card to be replaced")
        pickRelclick(origin, old_cards_region)
        time.sleep(np.random.rand()*2+.8)
    except:
        print("shuffling: something wasn't found")
        pass
    for _ in range(3):
        try:
            # find and click battle tab
            print("shuffling: looking for battle tab")
            loc_battle_tab = pgui.locateCenterOnScreen(menu_battle_tab, region=total_region, confidence=0.95)
            print("shuffling: found the battle tab")
            randclick(loc_battle_tab[0],loc_battle_tab[1])
            time.sleep(np.random.rand()*2+.8) # wait between .8 and 2.8 secs
        except:
            time.sleep(2)
            pass

    try:
        # find and click close text
        print("shuffling: looking for close text")
        loc_menu_close = pgui.locateCenterOnScreen(menu_close, region=total_region)
        print("shuffling: found close text")
        randclick(loc_menu_close[0],loc_menu_close[1], ydev=5)
        time.sleep(np.random.rand()*2+.8) # wait between .8 and 2.8 secs
    except:
        print("shuffling: close text not found")
        pass



for _ in range(1000):
    queue = Queue()
    startgame()
    playgamethread = threading.Thread(target=playgame, args=(queue,))
    screenshotthread = threading.Thread(target=take_screens, args=(total_region,1.,queue,))
    playgamethread.start()
    screenshotthread.start()
    playgamethread.join()
    screenshotthread.join()
    time.sleep(4)
    if _ % 5 == 0:
        print("shuffling")
        shuffle_cards()