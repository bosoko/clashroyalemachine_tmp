from keras.preprocessing.image import ImageDataGenerator
import numpy as np
import matplotlib.pyplot as plt


filename_avg = 'avg.npy'
dir_screenshots_train = "data\\screenshots\\train"
batch_size = 4096
# num_iters = 4000

train_datagen = ImageDataGenerator()

train_generator = train_datagen.flow_from_directory(
    directory=dir_screenshots_train,
    target_size=(130, 73),
    color_mode="rgb",
    batch_size=batch_size,
    class_mode="input",
) 

avg, _ = np.empty_like(next(train_generator))
avg = avg[0]

x_train, _ = next(train_generator)
for i in range(len(x_train)):
        avg += x_train[i]
        print(i)

avg /= batch_size

np.save(filename_avg, avg)

plt.imshow(avg)
plt.show()
# tst = x_train[15] - avg

# plt.imshow(tst)
# plt.show()


# x_train, _ = next(temp_generator)

# plt.imshow(x_train[5])
# plt.show()