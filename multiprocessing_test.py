import threading
import time
from queue import Queue
from PIL import Image
import pyautogui as pgui

def screenshots(queue):
    """worker function"""
    done = False
    i = 0
    while queue.empty():
        print ('Screenshots')
        time.sleep(1)
        i += 1
        sc = pgui.screenshot(region=(100,100,292,520)).resize()
        sc
    return

def game(queue):
    """worker function"""
    done = False
    i = 0
    while not done:
        print ('Game ')
        time.sleep(2)
        i += 1
        if i == 15:
            done = True
            queue.put(done)
    return


queue = Queue()
#if __name__ == '__main__':
threading.Thread(target=screenshots, args=(queue,)).start()
threading.Thread(target=game, args=(queue,)).start()
